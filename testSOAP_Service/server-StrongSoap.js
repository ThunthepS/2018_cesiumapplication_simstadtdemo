"use strict";

var soap = require('strong-soap').soap;
var fs = require('fs');
// wsdl of the web service this client is going to invoke. For local wsdl you can use, url = './wsdls/stockquote.wsdl'
var url = 'http://dmz02.rz.hft-stuttgart.de:8080/webestode/processes/WeBestEssen?wsdl';

// var requestArgs = {
//     WeBestEssenRequest : { gmlID: 'DENW22AL50000xNy',
//     attic_heating: true,
//     basement: true,
//     basement_heating: true,
//     building_type: 'EFH',
//     flat_roof: true,
//     function: '1010',
//     heated_volume: 123.5,
//     refurbishment: 'no',
//     storey_count: 1,
//     window_ratio: 0.25,
//     window_type: 0,
//     yoc: 1945
// }};
var requestArgs = {
    param0 : 'DENW22AL50000xNy',
    param1 : true,
    param2 : true,
    param3 : true,
    param4 : 'EFH',
    param5 : true,
    param6 : '1010',
    param7 : 123.5,
    param8 : 'no',
    param9 : 1,
    param10 : 0.25,
    param11 : 0,
    param12 : 1945
};
var options = {};
soap.createClient(url, options, function(err, client) {
  var method = client['WeBestEssen']['WeBestEssenPort']['process'];
  //console.log(client);
  var description = client.describe();
  //console.log(JSON.stringify(description.WeBestEssen.WeBestEssenPort.process));
  fs.writeFile('input', JSON.stringify(description.WeBestEssen.WeBestEssenPort.process.input), 'utf8');
  method(requestArgs, function(err, result, envelope, soapHeader) {
    //response envelope
    // console.log('Response Envelope: \n' + envelope);
    // //'result' is the response body
    // console.log('Result: \n' + JSON.stringify(result));
    fs.writeFile('result', JSON.stringify(result), 'utf8');
  });
});