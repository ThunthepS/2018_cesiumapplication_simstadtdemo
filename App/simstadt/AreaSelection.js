
var ii = 1;
var lastid;
var confirmArea = function () {
  if (ii == 1) {
    var select_id = $('#selectArea').find(":selected").text();
    lastid = select_id;
    console.log(select_id);
    var entity = neighborhoodEntities[select_id - 1];
    entity.polygon.material = Cesium.Color.RED.withAlpha(0.8);
    entity.viewFrom = new Cesium.Cartesian3(-450, -450, 500);
    viewer.trackedEntity = entity;
    ii = ii + 1;
  } else {
    var select_id = $('#selectArea').find(":selected").text();
    console.log(select_id);
    var entity = neighborhoodEntities[select_id - 1];
    entity.polygon.material = Cesium.Color.RED.withAlpha(0.8);
    entity.viewFrom = new Cesium.Cartesian3(-450, -450, 500);
    viewer.trackedEntity = entity;
    var lastentity = neighborhoodEntities[lastid - 1];
    lastentity.polygon.material = Cesium.Color.fromRandom({
      maximumRed: 0.5,
      maximumGreen: 0.5,
      minimumBlue: 0,
      alpha: 0.9
    });
    ii = ii + 1;
    lastid = select_id;
  }
};

$("#selectArea").change(function () {

  console.log("value change");
  console.log($('#selectArea').val());
});
var dataCSV;

var confirmAreaStat = function () {
  var select_id = $('#selectArea').find(":selected").text();
  //dataCSV = Papa.parse('./Data/stoeckach_data_2/data_clean.csv');
  Papa.parse("http://localhost:8000/App/Data/stoeckach_data_2/data_clean2.csv", {
    download: true,
    complete: function (results) {
      HightChartContainer.style.display = "block";
      console.log(results);
      dataCSV = results;
      var dataToPlot = [];
      var dataToPlot2 = [];
      for (let i = 0; i < 12; i++) {
        var temp = [Date.parse(results["data"][0][i]), Math.round(parseFloat(results["data"][select_id][i]))];
        dataToPlot.push(temp); //input the resultTime in Unix
        dataToPlot2.push(Math.round(parseFloat(results["data"][select_id][i])));
      }
      setTimeout(function cb() {
        //drawChartSingle(dataToPlot, "Hourly Heating Demand@Building Block ID " +select_id , "kWh");
        drawChartByMonth(dataToPlot2, "Monthly Heating Demand@Building Block ID " + select_id, "kWh");
        //drawChart(dataToPlot,dataToPlot,"","","","");
      }, 1000);
    }
  });
  console.log("To draw ID:" + select_id);
  //console.log( "Data:" + dataCSV);
};

var closeAreaStat = function () {
  HightChartContainer.style.display = "none";
  $("#EnergyBalance").hide("1000")
}
