//////////////////////////////////
// A Function to trigger the menu////
//////////////////////////////////

var open3DCMO = function () {
    if (DCMO.style.display === 'none') {
        //DCMO.style.display = "block";
        $("#DCMO").show("3000");
        AnalysisSec.style.display = 'none'
        HightChartContainer.style.display = "none";
        $("#TableContainer").hide();
    } else {
        DCMO.style.display = "none";
        HightChartContainer.style.display = "none";
        $("#TableContainer").hide();
    }
}

var openAnalysis = function () {
    if (AnalysisSec.style.display === 'none') {
        //AnalysisSec.style.display = "block";
        $("#AnalysisSec").show("3000");
        HightChartContainer.style.display = "none";
        DCMO.style.display = "none";
        $("#TableContainer").hide();
    } else {
        AnalysisSec.style.display = "none";
        HightChartContainer.style.display = "none";
        $("#TableContainer").hide();
    }
}

$("#analysisMode").change(function () {
    var select_mode = $('#analysisMode').find(":selected").text();
    if (select_mode == "Energy Balance") {
        //EnergyBalance.style.display = "block";
        $("#EnergyBalance").show("3000");
    }
    if (select_mode == "Option_2") {
        //EnergyBalance.style.display = "none";
        $("#EnergyBalance").hide("1000");
        $("#HightChartContainer").hide();
        $("#TableContainer").hide();
    }
    if (select_mode == "Option_3") {
        EnergyBalance.style.display = "none";
        //EnergyBalance.style.display = "none";
        $("#EnergyBalance").hide("1000");
        $("#HightChartContainer").hide();
        $("#TableContainer").hide();
    }
});

// $(document).ready(function(){
//     $(".img_menu").click(function(){
//         $("#menu").fadeIn("3000");
//     });
// });
var viewTable = function (){
    if (TableContainer.style.display === 'none') {
        
    $("#TableContainer").fadeIn("3000");
} else {
    //menu.style.display = "none"
    $("#TableContainer").fadeOut("3000");
}
}
var displayMenu = function(){
    $("#TableContainer").hide();
    $("#HightChartContainer").hide();
    if (menu.style.display === 'none') {
        //menu.style.display = "block";
        $("#menu").fadeIn("3000");
    } else {
        //menu.style.display = "none"
        $("#menu").fadeOut("3000");
    }
}

var RemoveStat = function () {
    $("#HightChartContainer").hide("1000")
}

var RemoveStatValue = function () {
    StatInfo.style.display = "none";
}

var closeAreaStat = function () {
    $("#EnergyBalance").hide("1000")
    $("#TableContainer").hide("1000")
    $("#HightChartContainer").hide("1000")
  }
  