var xmlresponse;
var xmlresponse_json;
var xml_resultText;
var xml_resultText_json, xml_resultText_temp;
var xml_resultText_csv;
var xmlresponse_heat = [];
var xmlresponse_cool = [];
// var input_gmlid;
// var input_attic_heating;
// var input_basement;
// var input_basement_heating;
// var input_building_type;
// var input_flat_roof;
// var input_function;
// var input_heated_volume;
// var input_refurbishment;
// var input_storey_count;
// var input_window_ratio;
// var input_window_type;
// var input_yoc;
$(document).ready(function () {
//     input_gmlid = $("#gmlid").val();
// input_attic_heating = $("#ath").val();
// input_basement = $("#b").val();;
// input_basement_heating = $("#bh").val();
// input_building_type = $("#bt").val();
// input_flat_roof = $("#fr").val();
// input_function = $("#f").val();
// input_heated_volume = $("#hv").val();
// input_refurbishment = $("#r").val();
// input_storey_count = $("#sc").val();
// input_window_ratio = $("#wr").val();
// input_window_type = $("#wt").val();
// input_yoc = $("#y").val();
});

var Soap_Req = function () {
    HightChartContainer.style.display = "none";
    $.ajax({
        url: "http://dmz02.rz.hft-stuttgart.de:8080/webestode/processes/WeBestEssen?wsdl",
        headers: {
        "Access-Control-Allow-Origin": "*"
    },
        crossDomain: true,
        type: "POST",
        dataType: "xml",
        data: "<s11:Envelope xmlns:s11='http://schemas.xmlsoap.org/soap/envelope/'><s11:Body><ns1:WeBestEssenRequest xmlns:ns1='http://webestessen'><ns1:gmlID>"+ $("#gmlid").val() +
        "</ns1:gmlID><ns1:basement>"+ $("#b").val() +
        "</ns1:basement><ns1:attic_heating>"+ $("#ath").val() +
        "</ns1:attic_heating><ns1:basement_heating>"+ $("#bh").val() +
        "</ns1:basement_heating><ns1:building_type>"+ $("#bt").val() +
        "</ns1:building_type><ns1:flat_roof>"+ $("#fr").val() +
        "</ns1:flat_roof><ns1:function>"+ $("#f").val() +
        "</ns1:function><ns1:heated_volume>"+ $("#hv").val() +
        "</ns1:heated_volume><ns1:refurbishment>"+ $("#r").val() +
        "</ns1:refurbishment><ns1:storey_count>"+ $("#sc").val() +
        "</ns1:storey_count><ns1:window_ratio>"+  $("#wr").val() +
        "</ns1:window_ratio><ns1:window_type>"+ $("#wt").val() +
        "</ns1:window_type><ns1:yoc>"+ $("#y").val() +
        "</ns1:yoc></ns1:WeBestEssenRequest></s11:Body></s11:Envelope>",
        // data: "<s11:Envelope xmlns:s11='http://schemas.xmlsoap.org/soap/envelope/'><s11:Body><ns1:WeBestEssenRequest xmlns:ns1='http://webestessen'><ns1:gmlID></ns1:gmlID><ns1:basement>true</ns1:basement><ns1:attic_heating>true</ns1:attic_heating><ns1:basement_heating>true</ns1:basement_heating><ns1:building_type>EFH</ns1:building_type><ns1:flat_roof>true</ns1:flat_roof><ns1:function>1010</ns1:function><ns1:heated_volume>123.5</ns1:heated_volume><ns1:refurbishment>no</ns1:refurbishment><ns1:storey_count>1</ns1:storey_count><ns1:window_ratio>0.25</ns1:window_ratio><ns1:window_type>0</ns1:window_type><ns1:yoc>1945</ns1:yoc></ns1:WeBestEssenRequest></s11:Body></s11:Envelope>",
        processData: true,
        contentType: "text/xml; charset=\"utf-8\"",
        success: function (xml) {
            console.log("Remark** to check the Respone body (Please check variable (xml_resultText_temp) [joe]");
            try {

            
            var xmlresponse_heat_temp = []; 
            var xmlresponse_cool_temp = [];
            var xmlresponse_temp = xml2json(xml);
            var xmlresponse_temp_clean = xmlresponse_temp.replace('undefined', '');
            var xmlresponse_json = JSON.parse(xmlresponse_temp_clean);
            var xml_resultText_temp = xmlresponse_json["soapenv:Envelope"]["soapenv:Body"]["WeBestEssenResponse"]["tns:result"];
            var xml_resultText_temp1 = xml_resultText_temp.replace(/      /g, ',');
            var xml_resultText_temp2 = xml_resultText_temp1.replace(/E-01   /g, ',');
            var xml_resultText_temp3 = xml_resultText_temp2.replace(/E-02   /g, ',');
            var xml_resultText_temp4 = xml_resultText_temp3.replace(/E-03   /g, ',');
            var xml_resultText_temp5 = xml_resultText_temp4.replace(/E-04   /g, ',');
            var xml_resultText_temp6 = xml_resultText_temp5.replace(/E-05   /g, ',');
            var xml_resultText_temp7 = xml_resultText_temp6.replace(/E-06   /g, ',');
            var xml_resultText_temp8 = xml_resultText_temp7.replace(/E-07   /g, ',');
            var xml_resultText_temp9 = xml_resultText_temp8.replace(/E-08   /g, ',');
            var xml_resultText_temp10 = xml_resultText_temp9.replace(/E-09   /g, ',');
            var xml_resultText_temp11 = xml_resultText_temp10.replace(/E-10   /g, ',');
            // xml_resultText_temp3 = xml_resultText_temp2.replace(/\n/g,';');
            // xml_resultText_temp4 = "{" + xml_resultText_temp3.slice(0,-2);
            // xml_resultText_temp5 = xml_resultText_temp4.replace(/ /g,'');
            xml_resultText_csv = Papa.parse(xml_resultText_temp11);
            setTimeout(function cb() {
                
                for (let i = 0; i <= 11; i++) {
                    xmlresponse_heat_temp.push(parseFloat(xml_resultText_csv["data"][i][0].trim()));
                    xmlresponse_cool_temp.push(parseFloat(xml_resultText_csv["data"][i][1].trim()));
                }
                xmlresponse_heat = xmlresponse_heat_temp;
                xmlresponse_cool = xmlresponse_cool_temp;
                console.log("Heat Demand : " + xmlresponse_heat_temp);
                console.log("Cool Demand : " + xmlresponse_cool_temp);
                

                //draw the chart
                HightChartContainer.style.display = "block";
                drawChart(xmlresponse_heat_temp, xmlresponse_cool_temp, "Heat Demand","Cool Demand","kWh/m²","kWh/m²");
                drawTable();
            }, 100);
            //xmlresponse_json1 = JSON.parse(xml_resultText_temp5);
            //xml_resultText = "HeatDemand,CoolDemand;" + xml_resultText_temp1.replace(/\n/g,';');
            //alert("it works");
            //console.log($(xml))
        } catch(err) {
            console.log("Response from Essen Service Error (Info :" + err + " )");
        }
        },
        error: function () { alert("Cross Origin Error => Temporaty workaround: Please, install the (Allow-Control-Allow-Origin: *) - CORS extension for chrome, find it here: https://goo.gl/QJ2yYW"); }
    })

}
var drawTable = function () {
    $('#BS_table').bootstrapTable({
        columns: [{
            field: 'id',
            title: 'Item ID'
        }, {
            field: 'JAN',
            title: 'JAN'
        }, {
            field: 'FEB',
            title: 'FEB'
        }, {
            field: 'MAR',
            title: 'MAR'
        }, {
            field: 'APR',
            title: 'APR'
        }, {
            field: 'MAY',
            title: 'MAY'
        }, {
            field: 'JUN',
            title: 'JUN'
        }, {
            field: 'JUL',
            title: 'JUL'
        }, {
            field: 'AUG',
            title: 'AUG'
        }, {
            field: 'SEP',
            title: 'SEP'
        }, {
            field: 'OCT',
            title: 'OCT'
        }, {
            field: 'NOV',
            title: 'NOV'
        }, {
            field: 'DEC',
            title: 'DEC'
        }],
        data: [{
            id: 'Heat Demand',
            JAN: xmlresponse_heat[0].toFixed(2),
            FEB: xmlresponse_heat[1].toFixed(2),
            MAR: xmlresponse_heat[2].toFixed(2),
            APR: xmlresponse_heat[3].toFixed(2),
            MAY: xmlresponse_heat[4].toFixed(2),
            JUN: xmlresponse_heat[5].toFixed(2),
            JUL: xmlresponse_heat[6].toFixed(2),
            AUG: xmlresponse_heat[7].toFixed(2),
            SEP: xmlresponse_heat[8].toFixed(2),
            OCT: xmlresponse_heat[9].toFixed(2),
            NOV: xmlresponse_heat[10].toFixed(2),
            DEC: xmlresponse_heat[11].toFixed(2) 
        }, {
            id: 'Cool Demand',
            JAN: xmlresponse_cool[0].toFixed(2),
            FEB: xmlresponse_cool[1].toFixed(2),
            MAR: xmlresponse_cool[2].toFixed(2),
            APR: xmlresponse_cool[3].toFixed(2),
            MAY: xmlresponse_cool[4].toFixed(2),
            JUN: xmlresponse_cool[5].toFixed(2),
            JUL: xmlresponse_cool[6].toFixed(2),
            AUG: xmlresponse_cool[7].toFixed(2),
            SEP: xmlresponse_cool[8].toFixed(2),
            OCT: xmlresponse_cool[9].toFixed(2),
            NOV: xmlresponse_cool[10].toFixed(2),
            DEC: xmlresponse_cool[11].toFixed(2) 
        }]
    });
};
// a function to draw Chart According to soap Request
var drawChart = function (DS_1, DS_2, dataTag1, dataTag2, unit1, unit2) {
    //setTimeout(function cb() {
    //var nameTag = 'E-bike ' + ebikeID;
    var nameTag = '';
    Highcharts.chart('HightChartContainer', {
        chart: {
            zoomType: 'x'
        }, title: {
            text: 'Heating Demand Value'
        },
        subtitle: {
            text: '[' + dataTag1 + ' and ' + dataTag2 + ']'
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: [{
            title: {
                text: dataTag1
            },
            labels: {
                format: '{value} ' + unit1
            }
        },
        {
            title: {
                text: dataTag2
            },
            opposite: true,
            labels: {
                format: '{value} ' + unit2
            }
        }],
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.y:.2f} ' + ' at {point.x:%H:%M:%S.%L}',
            crosshairs: true,
            shared: true
            //valueSuffix: ' {series.su}'
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: true,
                    radius: 2
                },
                series: {
                    lineWidth: 1
                }
            }
        },

        series: [{
            name: nameTag + dataTag1,
            yAxis: 0,
            type: 'spline',
            // Define the data points. All series have a dummy year
            // of 1970/71 in order to be compared on the same x axis. Note
            // that in JavaScript, months start at 0 for January, 1 for February etc.
            data: DS_1
        }, {
            name: nameTag + dataTag2,
            //yAxis: 1,
            // Define the data points. All series have a dummy year
            // of 1970/71 in order to be compared on the same x axis. Note
            // that in JavaScript, months start at 0 for January, 1 for February etc.
            data: DS_2,
            type: 'spline'
        }
        ]


    });
    //}, 2000);
}

var drawChartSingle = function (DS_1, dataTag1, unit1) {
    var nameTag = '';
    Highcharts.chart('HightChartContainer', {
        chart: {
            zoomType: 'x'
        }, title: {
            text: 'Heating Demand Chart'
        },
        subtitle: {
            text: '[' + dataTag1 + ']'
        },
        credits: {
            enabled: false
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year    
                minute: '%H:%M',
                hour: '%H:%M',
                month:"%B"
            },
            title: {
                text: 'Month (on year 2014)'
            }
        },
        yAxis: [{
            title: {
                text: 'Monthly Heating Demand (' +unit1 + ')'
            },
            labels: {
                format: '{value}'
            }
        }],
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.y:.2f} ' + ' at {point.x:%H:%M:%S.%L}',
            crosshairs: true,
            shared: true
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: true,
                    radius: 2
                },
                series: {
                    lineWidth: 1
                }
            }
        },

        series: [{
            name: nameTag + dataTag1,
            yAxis: 0,
            type: 'spline',
            data: DS_1
        }
        ]


    });

}

var drawChartByMonth = function (DS_1, dataTag1, unit1) {
    var nameTag = '';
    Highcharts.chart('HightChartContainer', {
        chart: {
            zoomType: 'x'
        }, title: {
            text: 'Heating Demand [2014]'
        },
        subtitle: {
            text: '[' + dataTag1 + ']'
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: [{
            title: {
                text: 'Monthly Heating Demand (' +unit1 + ')'
            },
            labels: {
                format: '{value}'
            }
        }],
        tooltip: {
            headerFormat: 'Heating Demand:',
            pointFormat: '{point.y:.2f} ' + 'kWh',
            crosshairs: true,
            shared: true
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: true,
                    radius: 2
                },
                series: {
                    lineWidth: 1
                }
            }
        },

        series: [{
            name: nameTag + dataTag1,
            yAxis: 0,
            type: 'spline',
            data: DS_1
        }
        ]


    });

}


function xml2json(xml, tab) {
    var X = {
        toObj: function (xml) {
            var o = {};
            if (xml.nodeType == 1) {   // element node ..
                if (xml.attributes.length)   // element with attributes  ..
                    for (var i = 0; i < xml.attributes.length; i++)
                        o["@" + xml.attributes[i].nodeName] = (xml.attributes[i].nodeValue || "").toString();
                if (xml.firstChild) { // element has child nodes ..
                    var textChild = 0, cdataChild = 0, hasElementChild = false;
                    for (var n = xml.firstChild; n; n = n.nextSibling) {
                        if (n.nodeType == 1) hasElementChild = true;
                        else if (n.nodeType == 3 && n.nodeValue.match(/[^ \f\n\r\t\v]/)) textChild++; // non-whitespace text
                        else if (n.nodeType == 4) cdataChild++; // cdata section node
                    }
                    if (hasElementChild) {
                        if (textChild < 2 && cdataChild < 2) { // structured element with evtl. a single text or/and cdata node ..
                            X.removeWhite(xml);
                            for (var n = xml.firstChild; n; n = n.nextSibling) {
                                if (n.nodeType == 3)  // text node
                                    o["#text"] = X.escape(n.nodeValue);
                                else if (n.nodeType == 4)  // cdata node
                                    o["#cdata"] = X.escape(n.nodeValue);
                                else if (o[n.nodeName]) {  // multiple occurence of element ..
                                    if (o[n.nodeName] instanceof Array)
                                        o[n.nodeName][o[n.nodeName].length] = X.toObj(n);
                                    else
                                        o[n.nodeName] = [o[n.nodeName], X.toObj(n)];
                                }
                                else  // first occurence of element..
                                    o[n.nodeName] = X.toObj(n);
                            }
                        }
                        else { // mixed content
                            if (!xml.attributes.length)
                                o = X.escape(X.innerXml(xml));
                            else
                                o["#text"] = X.escape(X.innerXml(xml));
                        }
                    }
                    else if (textChild) { // pure text
                        if (!xml.attributes.length)
                            o = X.escape(X.innerXml(xml));
                        else
                            o["#text"] = X.escape(X.innerXml(xml));
                    }
                    else if (cdataChild) { // cdata
                        if (cdataChild > 1)
                            o = X.escape(X.innerXml(xml));
                        else
                            for (var n = xml.firstChild; n; n = n.nextSibling)
                                o["#cdata"] = X.escape(n.nodeValue);
                    }
                }
                if (!xml.attributes.length && !xml.firstChild) o = null;
            }
            else if (xml.nodeType == 9) { // document.node
                o = X.toObj(xml.documentElement);
            }
            else
                alert("unhandled node type: " + xml.nodeType);
            return o;
        },
        toJson: function (o, name, ind) {
            var json = name ? ("\"" + name + "\"") : "";
            if (o instanceof Array) {
                for (var i = 0, n = o.length; i < n; i++)
                    o[i] = X.toJson(o[i], "", ind + "\t");
                json += (name ? ":[" : "[") + (o.length > 1 ? ("\n" + ind + "\t" + o.join(",\n" + ind + "\t") + "\n" + ind) : o.join("")) + "]";
            }
            else if (o == null)
                json += (name && ":") + "null";
            else if (typeof (o) == "object") {
                var arr = [];
                for (var m in o)
                    arr[arr.length] = X.toJson(o[m], m, ind + "\t");
                json += (name ? ":{" : "{") + (arr.length > 1 ? ("\n" + ind + "\t" + arr.join(",\n" + ind + "\t") + "\n" + ind) : arr.join("")) + "}";
            }
            else if (typeof (o) == "string")
                json += (name && ":") + "\"" + o.toString() + "\"";
            else
                json += (name && ":") + o.toString();
            return json;
        },
        innerXml: function (node) {
            var s = ""
            if ("innerHTML" in node)
                s = node.innerHTML;
            else {
                var asXml = function (n) {
                    var s = "";
                    if (n.nodeType == 1) {
                        s += "<" + n.nodeName;
                        for (var i = 0; i < n.attributes.length; i++)
                            s += " " + n.attributes[i].nodeName + "=\"" + (n.attributes[i].nodeValue || "").toString() + "\"";
                        if (n.firstChild) {
                            s += ">";
                            for (var c = n.firstChild; c; c = c.nextSibling)
                                s += asXml(c);
                            s += "</" + n.nodeName + ">";
                        }
                        else
                            s += "/>";
                    }
                    else if (n.nodeType == 3)
                        s += n.nodeValue;
                    else if (n.nodeType == 4)
                        s += "<![CDATA[" + n.nodeValue + "]]>";
                    return s;
                };
                for (var c = node.firstChild; c; c = c.nextSibling)
                    s += asXml(c);
            }
            return s;
        },
        escape: function (txt) {
            return txt.replace(/[\\]/g, "\\\\")
                .replace(/[\"]/g, '\\"')
                .replace(/[\n]/g, '\\n')
                .replace(/[\r]/g, '\\r');
        },
        removeWhite: function (e) {
            e.normalize();
            for (var n = e.firstChild; n;) {
                if (n.nodeType == 3) {  // text node
                    if (!n.nodeValue.match(/[^ \f\n\r\t\v]/)) { // pure whitespace text node
                        var nxt = n.nextSibling;
                        e.removeChild(n);
                        n = nxt;
                    }
                    else
                        n = n.nextSibling;
                }
                else if (n.nodeType == 1) {  // element node
                    X.removeWhite(n);
                    n = n.nextSibling;
                }
                else                      // any other node
                    n = n.nextSibling;
            }
            return e;
        }
    };
    if (xml.nodeType == 9) // document node
        xml = xml.documentElement;
    var json = X.toJson(X.toObj(X.removeWhite(xml)), xml.nodeName, "\t");
    return "{\n" + tab + (tab ? json.replace(/\t/g, tab) : json.replace(/\t|\n/g, "")) + "\n}";
}