


// HTML overlay for showing feature name on mouseover
var nameOverlay = document.createElement('div');
viewer.container.appendChild(nameOverlay);
nameOverlay.className = 'backdrop';
nameOverlay.style.display = 'none';
nameOverlay.style.position = 'absolute';
nameOverlay.style.bottom = '0';
nameOverlay.style.left = '0';
nameOverlay.style['pointer-events'] = 'none';
nameOverlay.style.padding = '4px';
nameOverlay.style.backgroundColor = 'black';
nameOverlay.style.width = '10px';


// Information about the currently selected feature
var selected = {
    feature: undefined,
    originalColor: new Cesium.Color()
};

// An entity object which will hold info about the currently selected feature for infobox display
var selectedEntity = new Cesium.Entity();

// Get default left click handler for when a feature is not picked on left click
var clickHandler = viewer.screenSpaceEventHandler.getInputAction(Cesium.ScreenSpaceEventType.LEFT_CLICK);

// If silhouettes are supported, silhouette features in blue on mouse over and silhouette green on mouse click.
// If silhouettes are not supported, change the feature color to yellow on mouse over and green on mouse click.

    // Silhouettes are not supported. Instead, change the feature color.

    // Information about the currently highlighted feature
    var highlighted = {
        feature : undefined,
        originalColor : new Cesium.Color()
    };

    // Color a feature yellow on hover.
    viewer.screenSpaceEventHandler.setInputAction(function onMouseMove(movement) {
        // If a feature was previously highlighted, undo the highlight
        if (Cesium.defined(highlighted.feature)) {
            highlighted.feature.color = highlighted.originalColor;
            highlighted.feature = undefined;
        }
        // Pick a new feature
        var pickedFeature = viewer.scene.pick(movement.endPosition);
        if (!Cesium.defined(pickedFeature)) {
            nameOverlay.style.display = 'none';
            return;
        }
        // A feature was picked, so show it's overlay content
        nameOverlay.style.display = 'block';
        nameOverlay.style.bottom = viewer.canvas.clientHeight - movement.endPosition.y + 'px';
        nameOverlay.style.left = movement.endPosition.x + 'px';
        var name = pickedFeature.getProperty('gmlIdALKISLageBezeichnung_1');
        if (!Cesium.defined(name)) {
            name = pickedFeature.getProperty('id');
        }
        nameOverlay.textContent = name;
        // Highlight the feature if it's not already selected.
        if (pickedFeature !== selected.feature) {
            highlighted.feature = pickedFeature;
            Cesium.Color.clone(pickedFeature.color, highlighted.originalColor);
            pickedFeature.color = Cesium.Color.YELLOW;
        }
    }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);

    // Color a feature on selection and show metadata in the InfoBox.
    viewer.screenSpaceEventHandler.setInputAction(function onLeftClick(movement) {
        // If a feature was previously selected, undo the highlight
        if (Cesium.defined(selected.feature)) {
            selected.feature.color = selected.originalColor;
            selected.feature = undefined;
        }
        // Pick a new feature
        var pickedFeature = viewer.scene.pick(movement.position);
        if (!Cesium.defined(pickedFeature)) {
            clickHandler(movement);
            return;
        }
        // Select the feature if it's not already selected
        if (selected.feature === pickedFeature) {
            return;
        }
        selected.feature = pickedFeature;
        // Save the selected feature's original color
        if (pickedFeature === highlighted.feature) {
            Cesium.Color.clone(highlighted.originalColor, selected.originalColor);
            highlighted.feature = undefined;
        } else {
            Cesium.Color.clone(pickedFeature.color, selected.originalColor);
        }
        // Highlight newly selected feature
        pickedFeature.color = Cesium.Color.LIME;
        addGMLid(pickedFeature.getProperty('GMLID_copy'),pickedFeature.getProperty('YearOfConstruction'),pickedFeature.getProperty('Function'));
        // Set feature infobox description
        var featureName = pickedFeature.getProperty('id');
        selectedEntity.name = featureName;
        selectedEntity.description = 'Loading <div class="cesium-infoBox-loading"></div>';
        viewer.selectedEntity = selectedEntity;
        selectedEntity.description = '<table class="cesium-infoBox-defaultTable"><tbody>' +
                                     '<tr><th>GMLID</th><td>' + pickedFeature.getProperty('GMLID_copy') + '</td></tr>' +
                                     //'<tr><th>GMLID</th><td>' + pickedFeature.getPropertyNames() + '</td></tr>' +
                                     '<tr><th>FeatureType</th><td>' + pickedFeature.getProperty('FeatureType') + '</td></tr>' +
                                     //'<tr><th>Geschoss Fläche</th><td>' + pickedFeature.getProperty('geschossflaeche') + '</td></tr>' +
                                     '<tr><th>Function</th><td>' + pickedFeature.getProperty('Function') + '</td></tr>' +
                                     '<tr><th>ALKIS ID</th><td>' + pickedFeature.getProperty('alkisId') + '</td></tr>' +
                                     //'<tr><th>Grundfläche</th><td>' + pickedFeature.getProperty('grundflaeche') + '</td></tr>' +
                                     '<tr><th>Year of Construction</th><td>' + pickedFeature.getProperty('YearOfConstruction') + '</td></tr>' +
                                     //'<tr><th>hoeheEFH</th><td>' + pickedFeature.getProperty('hoeheEFH') + '</td></tr>' +
                                     //'<tr><th>RoofType</th><td>' + pickedFeature.getProperty('RoofType') + '</td></tr>' +
                                     //'<tr><th>StoreysBelowGround</th><td>' + pickedFeature.getProperty('StoreysBelowGround') + '</td></tr>' +
                                     //'<tr><th>StoreysAboveGround</th><td>' + pickedFeature.getProperty('StoreysAboveGround') + '</td></tr>' +
                                     '</tbody></table>';

    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);


function addGMLid(id,y,f) {
    document.getElementById("gmlid").value = id;
    document.getElementById("y").value = y;
    document.getElementById("f").value = f;
    
};