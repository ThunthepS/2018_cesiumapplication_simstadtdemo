//https://github.com/mikolalysenko/cdt2d
//-------------------Pick Building -------------------------------------
function test(){
var clickHandler = viewer.screenSpaceEventHandler.getInputAction(Cesium.ScreenSpaceEventType.LEFT_CLICK);
viewer.screenSpaceEventHandler.setInputAction(function onLeftClick(movement){
    if (Cesium.defined(selected.feature)) {
        selected.feature.color = sleected.originalColor;
        selected.feature = undefined;
    }

    var pickedFeature = viewer.scene.pick(movement.position);
    if (!Cesium.defined(pickedFeature)) {
        clickHandler(movement);
        return;
    }
    if(selected.feature === pickedFeature) {
        return;
    }
    selected.feature = pickedFeature;

    if (pickedFeature === highlighted.feature) {
        Cesium.Color.clone(highlighted.originalColor, selected.originalColor);
        highlighted.feature = undefined;
    } else {
        Cesium.Color.clone(pickedFeature.color, selected.originalColor);
    }
    pickedFeature.color = Cesium.Color.PURPLE;

var featureName = pickFeature.getProperty('gmlID')
selectedEntity.name = featureName;
selectedEntity.description = 'Loading <div class="cesium-infoBox-loading"></div>';
viewer.selectedEntity = selectedEntity;
selectedEntity.description = '<table class="cesium-infoBox-defaultTable"><tbody>' +
                             '<tr><th>ALL</th><td>' + pickedFeature.getPropertyNames() + '</td></tr>' +
                             '</tbody></table>'
}, Cesium.ScreenSpaceEventType.LEFT_CLICK);
};
function ownArea() {
    
    // ------------------Pick Position --------------------------------------

        var entity = viewer.entities.add({
            label : {
                show : false,
                showBackground : true,
                font : '14px monospace',
                horizontalOrigin : Cesium.HorizontalOrigin.LEFT,
                verticalOrigin : Cesium.VerticalOrigin.TOP,
                pixelOffset : new Cesium.Cartesian2(15, 0)
            }
        });
    
        // Mouse over the globe to see the cartographic position
        handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
        handler.setInputAction(function(movement) {
            var cartesian = viewer.camera.pickEllipsoid(movement.endPosition, scene.globe.ellipsoid);
            //console.log(cartesian);
            if (cartesian) {
                var cartographic = Cesium.Cartographic.fromCartesian(cartesian);
                var longitudeString = Cesium.Math.toDegrees(cartographic.longitude).toFixed(2);
                var latitudeString = Cesium.Math.toDegrees(cartographic.latitude).toFixed(2);
    
                entity.position = cartesian;
                entity.label.show = true;
                entity.label.text =
                    'Lon: ' + ('   ' + longitudeString).slice(-7) + '\u00B0' +
                    '\nLat: ' + ('   ' + latitudeString).slice(-7) + '\u00B0';
            } else {
                entity.label.show = false;
            }
        }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);


    //------------------ Drawing --------------------------------------------------
    viewer.cesiumWidget.screenSpaceEventHandler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK);
    function createPoint(worldPosition) {
        var point = viewer.entities.add({
            position : worldPosition,
            point : {
                color : Cesium.Color.WHITE,
                pixelSize : 5,
                heightReference: Cesium.HeightReference.CLAMP_TO_GROUND
            }
        });
        return point;
    }
    var drawingMode = 'polygon';
    function drawShape(positionData) {
        var shape;


            shape = viewer.entities.add({
                polygon: {
                    hierarchy: positionData,
                    material: new Cesium.ColorMaterialProperty(Cesium.Color.WHITE.withAlpha(0.5))
                }
            });

        return shape;
    }
    function finishShape(positionData) {
        var shape;


            shape = viewer.entities.add({
                polygon: {
                    hierarchy: positionData,
                    material: new Cesium.ColorMaterialProperty(Cesium.Color.RED.withAlpha(0.5))
                }
            });

        return shape;
    }
    var activeShapePoints = [];
    var activeShape;
    var floatingPoint;
    var handler = new Cesium.ScreenSpaceEventHandler(viewer.canvas);
    handler.setInputAction(function(event) {
        // We use `viewer.scene.pickPosition` here instead of `viewer.camera.pickEllipsoid` so that
        // we get the correct point when mousing over terrain.
        var earthPosition = viewer.camera.pickEllipsoid(event.position, scene.globe.ellipsoid);
        // `earthPosition` will be undefined if our mouse is not over the globe.
        if (Cesium.defined(earthPosition)) {
            if (activeShapePoints.length === 0) {
                floatingPoint = createPoint(earthPosition);
                activeShapePoints.push(earthPosition);
                var dynamicPositions = new Cesium.CallbackProperty(function () {
                    return activeShapePoints;
                }, false);
                activeShape = drawShape(dynamicPositions);
            }
            activeShapePoints.push(earthPosition);
            createPoint(earthPosition);
        }
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
    
    handler.setInputAction(function(event) {
        if (Cesium.defined(floatingPoint)) {
            var newPosition = viewer.camera.pickEllipsoid(event.endPosition, scene.globe.ellipsoid);
            //console.log(newPosition);
            //viewer.camera.pickEllipsoid(event.position, scene.globe.ellipsoid);
            if (Cesium.defined(newPosition)) {
                floatingPoint.position.setValue(newPosition);
                activeShapePoints.pop();
                activeShapePoints.push(newPosition);
            }
        }
    }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
    // Redraw the shape so it's not dynamic and remove the dynamic shape.
    function terminateShape() {
        activeShapePoints.pop();
        finishShape(activeShapePoints);
        viewer.entities.remove(floatingPoint);
        viewer.entities.remove(activeShape);
        floatingPoint = undefined;
        activeShape = undefined;
        activeShapePoints = [];
    }
    handler.setInputAction(function(event) {
        getBBox();
        terminateShape();
        //polygontest();
        //loadTileset(url,maxx,maxy,minx,miny)
    }, Cesium.ScreenSpaceEventType.RIGHT_CLICK);
    
    var options = [{
        text : 'Draw Polygons',
        onselect : function() {
            
            terminateShape();
            drawingMode = 'polygon';
        }
    }];


    //------------------------Color Buildings -----------------------------------------------
    var newcount = 2
    function getBBox() {
        for(var i = 0; i < activeShapePoints.length -1; i++){

            datap[newcount] = activeShapePoints[i].x;
            datap[newcount + 1] = activeShapePoints[i].y;
            newcount += 2;

        };
        console.log(datap);
    };



    //--------------------------------------------------------------------------------------
};
    //--------------------------------------------------------------------------------------
    var tileset;
    var expres;
    var url = 'http://localhost:8000/App/Data/Stuttgart3D_2/3dTiles';

    function loadTileset() {
        city.show = false;
        tileset = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
            url: url

        }));
        
        //expres = "${Longitude} >= " + x1 + " && ${Latitude} >= " + y1 + " && ${Longitude} <= " + x2 + " && ${Latitude} <= " + y2;
          

        tileset.style = new Cesium.Cesium3DTilesetStyle({
            //show: expres
            show: polygontest("${Longitude}", "${Latitude}") + " = -1"
        })


    };
    //---------------------------------------------------------------------------------------
    //----------------------------TEST-------------------------------------------------------
    var polygon = [ [ 1, 1 ], [ 1, 2 ], [ 2, 2 ], [ 2, 1 ] ]
    var point = [4155547.398551471, 673543.9523118217]
    var datap = {};
    let resdata;
    //datap[0] = point[0];
    //datap[1] = point[1];

    //console.log(polygontest());
    console.log(datap);
    function polygontest(pointx,pointy) {
        datap[0] = pointx;
        datap[1] = pointy;
        try {
            console.log(datap)
            $.ajax({
                type: "POST",
                url: 'http://localhost:8001/polygon',
                data: datap,
            }).done(function (newdata) {
                convertdata(newdata);
            });
            
            function convertdata(datanew){
                resdata = datanew;
                console.log(resdata)
            }
            return resdata;
        }
        catch (err) {
            console.log('delivering of the inserted tree data to the server failed!');
        }
        
    };

